$(document).ready(function(){
	var isAdded=false;
	var add_fav=$("#addFav");
	
    $("#addFav_container").click(function(){
    	isAdded=add_fav.attr("class")=="fa fa-heart"?true:false;
    	if (isAdded) {
    		add_fav.removeClass("fa fa-heart");
      		add_fav.addClass("fa fa-heart-o");
      		isAdded=false;
    	}else{
    		add_fav.removeClass("fa fa-heart-o");
      		add_fav.addClass("fa fa-heart");
      		isAdded=true;
    	}
      
    
    });
});